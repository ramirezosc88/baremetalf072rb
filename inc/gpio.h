#ifndef _GPIO_H_
#define _GPIO_H_
#include "types.h"
#include "register.h"
/*
 * GPIO PINS
 */
#define GPIO_PIN_0                 ((uint16)0x0001) 
#define GPIO_PIN_1                 ((uint16)0x0002)
#define GPIO_PIN_2                 ((uint16)0x0004) 
#define GPIO_PIN_3                 ((uint16)0x0008)
#define GPIO_PIN_4                 ((uint16)0x0010)
#define GPIO_PIN_5                 ((uint16)0x0020) 
#define GPIO_PIN_6                 ((uint16)0x0040) 
#define GPIO_PIN_7                 ((uint16)0x0080)
#define GPIO_PIN_8                 ((uint16)0x0100)
#define GPIO_PIN_9                 ((uint16)0x0200)
#define GPIO_PIN_10                ((uint16)0x0400)
#define GPIO_PIN_11                ((uint16)0x0800)
#define GPIO_PIN_12                ((uint16)0x1000)
#define GPIO_PIN_13                ((uint16)0x2000)
#define GPIO_PIN_14                ((uint16)0x4000)
#define GPIO_PIN_15                ((uint16)0x8000)
#define GPIO_PIN_All               ((uint16)0xFFFF)


/*
 * GPIO PIN MODES
 */
#define GPIO_MODE_INPUT            ((uint32)0x00000000)
#define GPIO_MODE_OUTPUT           ((uint32)0x00000001) 
#define GPIO_MODE_ANALOG           ((uint32)0x00000003)
#define GPIO_MODE_ALTERNATE        ((uint32)0x00000004)
#define GPIO_MODE_IT_FALLING       ((uint32)0x00000005)
#define GPIO_MODE_IT_RISING        ((uint32)0x00000006)
#define GPIO_MODE_IT_BOTH          ((uint32)0x00000007)

/*
 * GPIO OUTPUT TYPES
 */
#define GPIO_OP_TYPE_PP            ((uint32)0x00000000)
#define GPIO_OP_TYPE_OD            ((uint32)0x00000001) 

/*
 * GPIO OUTPUT SPEED
 */
#define GPIO_SPEED_LOW             ((uint32)0x00000000)
#define GPIO_SPEED_MEDIUM          ((uint32)0x00000001)
#define GPIO_SPEED_HIGH            ((uint32)0x00000002)

/*
 * GPIO PULLUP PULLDOWN
 */
#define  GPIO_NOPULL        (0x00000000)
#define  GPIO_PULLUP        (0x00000001)
#define  GPIO_PULLDOWN      (0x00000002)

#define GPIO_AF1_USART2     ((uint32)0x00000001)

typedef struct
{
    uint32 Pin;
    uint32 mode;
    uint32 type;      
    uint32 Pull;   
    uint32 speed;    
    uint32 Alternate;
}GPIOCfgTypeDef;


void gpio_configPort( GPIOTypeDef *port, GPIOCfgTypeDef *config );
void gpio_writePort( GPIOTypeDef *port, uint32 value );
uint32 gpio_readPort( GPIOTypeDef *port );
void gpio_setPins( GPIOTypeDef *port, uint32 pins );
void gpio_resetPins( GPIOTypeDef *port, uint32 pins );
void gpio_togglePins( GPIOTypeDef *port, uint32 pins );
void gpio_writePins( GPIOTypeDef *port, uint32 pins, uint32 value );
uint32 gpio_readPin( GPIOTypeDef *port, uint32 pin );

void gpio_isrHandler( uint32 pin );
void gpio_isrCallback( uint32 pin );


#endif