#ifndef _TYPES_H_
#define _TYPES_H_

#define SET   1
#define RESET 0

#define uint8  unsigned char
#define uint16 unsigned short
#define uint32 unsigned long
#define int8   signed   char
#define int16  signed   short
#define int32  signed   long
#define __IO   volatile 
#define weak   __attribute__((weak))

#define LEFT_SHIFT( var, bits )         ( (var) << (bits) ) 
#define RIGHT_SHIFT( var, bits )        ( (var) >> (bits) )
#define SET_BIT_TO_ONE( var, bit )     ( (var) |=  (1 << (bit)) )
#define SET_BIT_TO_CERO(var, bit)      ( (var) &= (~(1 << (bit))) )

#define TOGGLE_BIT(var,bit) ( ((1 << (bit) ) & (var) ) > 0 ? (var ^= (1 << (bit)) ) : ( (var) |= (1 << (bit))) ) 
#define BIT_STATE(var,bit) ( ((1 << (bit) ) & (var) ) > 0 ? 1 : 0 )
#define DELAY(count) for(volatile uint32 i = 0; i < (count); i++)

#define GET_GPIOX( GPIO)   ( (GPIO == GPIOA)? 0 : \
                             (GPIO == GPIOB)? 1 : \
                             (GPIO == GPIOC)? 2 : 0 )


#endif



