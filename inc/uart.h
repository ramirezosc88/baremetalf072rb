#ifndef _UART_H_
#define _UART_H_
#include "types.h"
#include "register.h"

#define UART_WORDLENGTH_7B   ((uint32)0x00000002)
#define UART_WORDLENGTH_8B   ((uint32)0x00000000)
#define UART_WORDLENGTH_9B   ((uint32)0x00000001)


#define UART_STOPBITS_0_5   1	
#define UART_STOPBITS_1   	0
#define UART_STOPBITS_1_5   3
#define UART_STOPBITS_2     2

#define UART_PARITY_NONE  0	
#define UART_PARITY_EVEN  1 	
#define UART_PARITY_ODD   2

#define UART_MODE_RX  	 1
#define UART_MODE_TX	 2
#define UART_MODE_TX_RX  3

#define UART_OVERSAMPLING_8   0
#define UART_OVERSAMPLING_16  1

typedef struct
{
    USARTTypeDef *uart;    
    uint32 BaudRate;                      
    uint32 WordLength;  	
    uint32 StopBits;  	
    uint32 Parity;
    //uint32 HwFlowCtl;
    uint32 Mode; 
    uint32 Oversampling;
}UARTTypeDef;


void uart_configPort( UARTTypeDef *uartH );
void uart_mspInit( UARTTypeDef *uartH );
void uart_sendBuffer( UARTTypeDef *uartH, uint8 *ptr, uint32 len );
void uart_sendBufferInt( UARTTypeDef *uartH, uint8 *ptr, uint32 len );
void uart_receiveBufferInt( UARTTypeDef *uartH, uint8 *ptr, uint32 len );
void uart_isrHandler( UARTTypeDef *uartH );
void uart_isrTxCallback( UARTTypeDef *uartH );
void uart_isrRxCallback( UARTTypeDef *uart );


#endif
