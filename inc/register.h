#ifndef _REGISTER_H_
#define _REGISTER_H_


#define FLASH_ADDR_BASE    0x08000000
#define SRAM_ADDR_BASE     0x20000000
#define APB_ADDR_BASE      0x40010000
#define AHB1_ADDR_BASE     0x40020000
#define AHB2_ADDR_BASE     0x48000000
#define GPIOA_ADDR_BASE    0x48000000
#define GPIOB_ADDR_BASE    0x48000400
#define GPIOC_ADDR_BASE    0x48000800
#define RCC_ADDR_BASE      0x40021000
#define NVIC_ADDR_BASE     0xE000E100
#define EXTI_ADDR_BASE     0x40010400 
#define SYSCFG_ADDR_BASE   0x40010000
#define USART1_ADDR_BASE   0x40013800
#define USART2_ADDR_BASE   0x40004400
#define USART3_ADDR_BASE   0x40004800
#define USART4_ADDR_BASE   0x40004C00
#define CLOCK_8MHZ         0x007A1200


typedef struct 
{
    __IO uint32 SYSCFG_CFGR1;
    uint32 RESERVED;
    __IO uint32 EXTICR[4];
    __IO uint32 CFGR2;
   
}SYSCFGTypeDef;

typedef struct 
{
    __IO uint32 IMR;
    __IO uint32 EMR;
    __IO uint32 RTSR;
    __IO uint32 FTSR;
    __IO uint32 SWIER;
    __IO uint32 PR;

}EXTITypeDef;

typedef struct 
{
    __IO  uint32 ISER;
    uint32 RESERV[31];
    __IO uint32 ICER;
    uint32 RESERV2[31];
    __IO uint32 ISPR;
    uint32 RESERV3[31];
    __IO uint32 ICPR;
    uint32 RESERV4[95];
    __IO uint32 IPR[8];
}NVICTypeDef;

typedef struct {
    __IO uint32 GPIOx_MODER;  
    __IO uint32 GPIOx_OTYPER;  
    __IO uint32 GPIOx_OSPEEDR;  
    __IO uint32 GPIOx_PUPDR;  
    __IO uint32 GPIOx_IDR;  
    __IO uint32 GPIOx_ODR;  
    __IO uint32 GPIOx_BSRR;  
    __IO uint32 GPIOx_LCKR;  
    __IO uint32 GPIOx_AFRL;  
    __IO uint32 GPIOx_AFRH;
    __IO uint32 GPIOx_BRR;

}GPIOTypeDef;

typedef struct{
  
    __IO uint32 RCC_CR;
    __IO uint32 RCC_CFGR;
    __IO uint32 RCC_CIR;
    __IO uint32 RCC_APB2RSTR;
    __IO uint32 RCC_APB1RSTR;
    __IO uint32 RCC_AHBENR;
    __IO uint32 RCC_APB2ENR;
    __IO uint32 RCC_APB1ENR;
    __IO uint32 RCC_BDCR;
    __IO uint32 RCC_CSR;
    __IO uint32 RCC_AHBRSTR;
    __IO uint32 RCC_CFGR2;
    __IO uint32 RCC_CFGR3;
    __IO uint32 RCC_CR2;
    
}RCCTypeDef;

typedef struct
{
  volatile uint32 CR1;     
  volatile uint32 CR2;   
  volatile uint32 CR3;    
  volatile uint32 BRR;   
  volatile uint32 GTPR;   
  volatile uint32 RTOR;    
  volatile uint32 RQR;   
  volatile uint32 ISR;    
  volatile uint32 ICR;    
  volatile uint32 RDR;                                               
  volatile uint32 TDR;    
} USARTTypeDef;

typedef enum
{
    NonMaskableInt_IRQn         = -14,
    HardFault_IRQn              = -13,
    SVC_IRQn                    = -5,
    PendSV_IRQn                 = -2,
    SysTick_IRQn                = -1,
    WWDG_IRQn                   = 0, 
    PVD_VDDIO2_IRQn             = 1, 
    RTC_IRQn                    = 2, 
    FLASH_IRQn                  = 3, 
    RCC_CRS_IRQn                = 4, 
    EXTI0_1_IRQn                = 5, 
    EXTI2_3_IRQn                = 6, 
    EXTI4_15_IRQn               = 7, 
    TSC_IRQn                    = 8, 
    DMA1_Channel1_IRQn          = 9, 
    DMA1_Channel2_3_IRQn        = 10,
    DMA1_Channel4_5_6_7_IRQn    = 11,
    ADC1_COMP_IRQn              = 12,
    TIM1_BRK_UP_TRG_COM_IRQn    = 13,
    TIM1_CC_IRQn                = 14,
    TIM2_IRQn                   = 15,
    TIM3_IRQn                   = 16,
    TIM6_DAC_IRQn               = 17,
    TIM7_IRQn                   = 18,
    TIM14_IRQn                  = 19,
    TIM15_IRQn                  = 20,
    TIM16_IRQn                  = 21,
    TIM17_IRQn                  = 22,
    I2C1_IRQn                   = 23,
    I2C2_IRQn                   = 24,
    SPI1_IRQn                   = 25,
    SPI2_IRQn                   = 26,
    USART1_IRQn                 = 27,
    USART2_IRQn                 = 28,
    USART3_4_IRQn               = 29,
    CEC_CAN_IRQn                = 30,
    USB_IRQn                    = 31 
} _irq_t;

#define GPIOA  ((GPIOTypeDef*)GPIOA_ADDR_BASE)
#define GPIOB  ((GPIOTypeDef*)GPIOB_ADDR_BASE)
#define GPIOC  ((GPIOTypeDef*)GPIOC_ADDR_BASE)
#define RCC    ((RCCTypeDef*)RCC_ADDR_BASE)
#define NVIC   ((NVICTypeDef*)NVIC_ADDR_BASE)
#define EXTI   ((EXTITypeDef*)EXTI_ADDR_BASE)
#define SYSCFG ((SYSCFGTypeDef*)SYSCFG_ADDR_BASE)
#define USART1 ((USARTTypeDef*)USART1_ADDR_BASE)
#define USART2 ((USARTTypeDef*)USART2_ADDR_BASE)
#define USART3 ((USARTTypeDef*)USART3_ADDR_BASE)
#define USART4 ((USARTTypeDef*)USART4_ADDR_BASE)

#endif