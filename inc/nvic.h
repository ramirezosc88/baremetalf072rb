#ifndef _NVIC_H_
#define _NVIC_H_
#include "types.h"
#include "register.h"

void nvic_SetPriority( _irq_t irq, uint32 priority );
uint32 nvic_GetPriority( _irq_t irq );
void nvic_EnableIrq( _irq_t irq );
void nvic_DisableIrq( _irq_t irq );
uint32 nvic_GetPendingIrq( _irq_t irq );
void nvic_SetPendingIrq( _irq_t irq );
void nvic_ClearPendingIrq( _irq_t irq );

#endif