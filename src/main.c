#include <stdio.h>
#include "types.h"
#include "rcc.h"
#include "gpio.h"
#include "nvic.h"
#include "uart.h"

UARTTypeDef UartInit;
//extern void initialise_monitor_handles(void);
void GPIO_Init(void);
void Uart_Init(void);


int main(void)
{
    uint8 bufferTx[] = "Hello\n";
    GPIO_Init();
    Uart_Init();
    for (;;)
    {
        DELAY(50000);
        uart_sendBuffer(&UartInit, bufferTx, 6);
        gpio_togglePins(GPIOA, GPIO_PIN_5);
    }
}

void GPIO_Init(void)         
{             
    RCC_GPIOA_ENABLE;
    GPIOCfgTypeDef GPIOInit; 
    GPIOInit.Pin = GPIO_PIN_5 | GPIO_PIN_4;
    GPIOInit.mode = GPIO_MODE_OUTPUT;   
    GPIOInit.Pull = GPIO_NOPULL;
    gpio_configPort(GPIOA, &GPIOInit);
    //gpio_writePins(GPIOA, GPIO_PIN_4 | GPIO_PIN_5, RESET);
}

void Uart_Init(void)
{
    UartInit.uart = USART2;
    UartInit.BaudRate = 9600;
    UartInit.Mode = UART_MODE_TX_RX;
    UartInit.WordLength = UART_WORDLENGTH_8B;
    UartInit.StopBits = UART_STOPBITS_1;
    UartInit.Parity = UART_PARITY_NONE;
    UartInit.Oversampling = UART_OVERSAMPLING_16;
    uart_configPort(&UartInit);
}

void uart_mspInit( UARTTypeDef *uartH )
{
	RCC_GPIOA_ENABLE;
	RCC_USART2_ENABLE;
	GPIOCfgTypeDef GPIO_InitStruct;
    GPIO_InitStruct.Pin = GPIO_PIN_2 | GPIO_PIN_3; 
    GPIO_InitStruct.mode = GPIO_MODE_ALTERNATE;
    GPIO_InitStruct.Pull = GPIO_PULLUP;        
    GPIO_InitStruct.speed = GPIO_SPEED_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF1_USART2;
    gpio_configPort(GPIOA, &GPIO_InitStruct);
}