#include "nvic.h"

void nvic_SetPriority( _irq_t irq, uint32 priority )
{
    if (priority < 4)                                                                          
    {     
        SET_BIT_TO_CERO( NVIC->IPR[RIGHT_SHIFT(irq, 2)],  LEFT_SHIFT(priority, ((irq % 4) * 8 ) + 6 ) );    
        SET_BIT_TO_CERO( NVIC->IPR[RIGHT_SHIFT(irq, 2)],  LEFT_SHIFT(priority, ((irq % 4) * 8 ) + 7 ) );                                                       
        NVIC->IPR[RIGHT_SHIFT(irq, 2)] |= LEFT_SHIFT(priority, (  ((irq % 4) * 8 ) + 6 ) ); 
    }
}

uint32 nvic_GetPriority( _irq_t irq )
{
    uint32 temp = NVIC->IPR[RIGHT_SHIFT(irq, 2)];
    return RIGHT_SHIFT( temp, ((irq % 4) * 8 ) + 6);
}

void nvic_EnableIrq( _irq_t irq )
{   
    SET_BIT_TO_ONE(NVIC->ISER, irq);            
}

void nvic_DisableIrq( _irq_t irq )
{
    SET_BIT_TO_ONE(NVIC->ICER, irq);
}

uint32 nvic_GetPendingIrq( _irq_t irq )
{
    return (uint32)BIT_STATE(NVIC->ISPR, irq);
}

void nvic_SetPendingIrq( _irq_t irq )
{
    SET_BIT_TO_ONE(NVIC->ISPR, irq);
}

void nvic_ClearPendingIrq( _irq_t irq )
{
    SET_BIT_TO_ONE(NVIC->ICPR, irq );
}