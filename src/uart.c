#include "uart.h"
#include "gpio.h"
#include "rcc.h"

void uart_configPort( UARTTypeDef *uartH )
{
	uint32 temp = 0;
/******************************** Configuration of CR1******************************************/
	uart_mspInit(uartH);
	//Enable USART Tx and Rx engines according to the USART_Mode configuration item
    if (uartH->Mode == UART_MODE_RX)
    {   //  32xx   01xx         
        //  TR
        SET_BIT_TO_ONE(temp, 2);
    }
    else if (uartH->Mode == UART_MODE_TX)
    {   //   32xx  10xx
        //   TR
        SET_BIT_TO_ONE(temp, 3);
    }
    else if (uartH->Mode == UART_MODE_TX_RX)
    {
        //Implement the code to enable the both Transmitter and Receiver bit fields 
		SET_BIT_TO_ONE(temp, 2);
        SET_BIT_TO_ONE(temp, 3);
    }
    //Implement the code to configure the Word length configuration item 

	if (uartH->WordLength == UART_WORDLENGTH_9B)
	{
		SET_BIT_TO_ONE(temp, 12 );
	}
	else if (uartH->WordLength == UART_WORDLENGTH_7B)
	{
		SET_BIT_TO_ONE(temp, 28 );
	}

    //Configuration of parity control bit fields
	if ( uartH->Parity == UART_PARITY_EVEN)
	{
		//Implement the code to enale the parity control 
		SET_BIT_TO_ONE(temp, 10);

	}else if ( uartH->Parity == UART_PARITY_ODD )
	{
		//Implement the code to enable the parity control 
	    SET_BIT_TO_ONE(temp, 10);
		SET_BIT_TO_ONE(temp, 9);
	}

	uartH->uart->CR1 = temp;

/******************************** Configuration of CR2******************************************/

	//Implement the code to configure the number of stop bits inserted during USART frame transmission 
	uartH->uart->CR2 &= ~(0x03u << 12);
	uartH->uart->CR2 |= uartH->StopBits << 12;
	
/******************************** Configuration of BRR(Baudrate register)******************************************/
	uint32 usartdiv;
	
	if (uartH->Oversampling == UART_OVERSAMPLING_8)
	{
		uint32 mask;
		usartdiv = (0x02 * CLOCK_8MHZ) / uartH->BaudRate;
		mask = (usartdiv % 16) >> 1;
		usartdiv -= (usartdiv % 16);
		usartdiv += mask;
		uartH->uart->BRR |= usartdiv;
	}
	else if ( uartH->Oversampling == UART_OVERSAMPLING_16)
	{	
		usartdiv = 8000000 / uartH->BaudRate;
		uartH->uart->BRR = usartdiv;
	}
/**********************************************  Enable USART  *****************************************/
	
	SET_BIT_TO_ONE( uartH->uart->CR1, 0);
	
	
}

void uart_sendBuffer( UARTTypeDef *uartH, uint8 *ptr, uint32 len )
{
    
	for (uint32 i = 0; i < len; i++)
	{
		uartH->uart->TDR = ptr[i];
		
		//while ( BIT_STATE( uartH->uart->ISR, 6) ); // TXE
		//while (  BIT_STATE( uartH->uart->ISR, 6 ) == 0 ); // TC
	}
}

void uart_sendBufferInt( UARTTypeDef *uartH, uint8 *ptr, uint32 len )
{

}

void uart_receiveBufferInt( UARTTypeDef *uartH, uint8 *ptr, uint32 len )
{

}

void uart_isrHandler( UARTTypeDef *uartH )
{

}

void uart_isrTxCallback( UARTTypeDef *uartH )
{

}

weak void uart_isrRxCallback( UARTTypeDef *uart )
{

}

weak void uart_mspInit( UARTTypeDef *uartH )
{
	
}