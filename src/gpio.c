#include "gpio.h"
#include "rcc.h"

void gpio_configPort(GPIOTypeDef *port, GPIOCfgTypeDef *config)
{
    uint32 position = 0;
    uint32 currentPin;

    while ((config->Pin) >> position != 0)
    {
        currentPin = (config->Pin) & (1 << position);

        if (currentPin != 0)
        {
            // configure the mode
            if (config->mode <= GPIO_MODE_ANALOG)
            {
                SET_BIT_TO_CERO(port->GPIOx_MODER, (position * 2));
                SET_BIT_TO_CERO(port->GPIOx_MODER, (position * 2) + 1);
                port->GPIOx_MODER |= (config->mode << (position * 2));
            }
            // configure the interrupt mode
            // else
            // {
            //     if (config->mode == GPIO_MODE_IT_FALLING)
            //     {
            //         SET_BIT_TO_ONE(EXTI->FTSR, position);
            //         SET_BIT_TO_CERO(EXTI->RTSR, position);
            //     }
            //     else if (config->mode == GPIO_MODE_IT_RISING)
            //     {
            //         SET_BIT_TO_ONE(EXTI->RTSR, position);
            //         SET_BIT_TO_CERO(EXTI->FTSR, position);
            //     }
            //     else if (config->mode == GPIO_MODE_IT_BOTH)
            //     {
            //         SET_BIT_TO_ONE(EXTI->FTSR, position);
            //         SET_BIT_TO_ONE(EXTI->RTSR, position);
            //     }

            //     RCC_SYSCFG_ENABLE;
                
            //     SYSCFG->EXTICR[RIGHT_SHIFT(position, 2)] &= ~(0x0F) << ( (position % 4) * 4 );
            //     SYSCFG->EXTICR[RIGHT_SHIFT(position, 2)] |= GET_GPIOX(port) << ( (position % 4) * 4 );
                
            //     SET_BIT_TO_ONE(EXTI->IMR, position);
            // }

            // congigure pullup or puldown
            SET_BIT_TO_CERO(port->GPIOx_PUPDR, (position * 2));
            SET_BIT_TO_CERO(port->GPIOx_PUPDR, (position * 2) + 1);
            port->GPIOx_PUPDR |= (config->Pull << (position * 2));

            // configure the speed
            // SET_BIT_TO_CERO(port->GPIOx_OSPEEDR, (position * 2));
            // SET_BIT_TO_CERO(port->GPIOx_OSPEEDR, (position * 2) + 1);
            // port->GPIOx_OSPEEDR |= (config->speed << (position * 2));

            //configure type
            SET_BIT_TO_CERO(port->GPIOx_OTYPER, position);
            port->GPIOx_OTYPER |= (config->type << position );

            if (config->mode == GPIO_MODE_ALTERNATE)
            {
                SET_BIT_TO_CERO(port->GPIOx_MODER, (position * 2));
                SET_BIT_TO_CERO(port->GPIOx_MODER, (position * 2) + 1);
                port->GPIOx_MODER |= (2 << (position * 2));
                
                if ( position < 8 )
                {
                    port->GPIOx_AFRL &= ~( 0x0F << (position * 4) ); 
                    port->GPIOx_AFRL |= config->Alternate << (position * 4);
                }
                else
                {
                    port->GPIOx_AFRH &= ~(0x0F << (position * 4));
                    port->GPIOx_AFRH |= config->Alternate << (position * 4);
                }
            }
        } // end if currentpin
        position++;
    } // end while
}

void gpio_resetPins(GPIOTypeDef *port, uint32 pins)
{
    port->GPIOx_BRR = (uint16)pins;
}

void gpio_togglePins(GPIOTypeDef *port, uint32 pins)
{
    if ((port->GPIOx_ODR & pins) != 0X00u)     
    {                                          
        port->GPIOx_BSRR = (uint32)pins << 16; 
    }                                          
    else
    {
        port->GPIOx_BSRR = (uint32)pins; 
    }
}

void gpio_writePort(GPIOTypeDef *port, uint32 value)
{
    port->GPIOx_ODR = value;
}

void gpio_writePins(GPIOTypeDef *port, uint32 pins, uint32 value)
{
    if (value == SET)
    {
        port->GPIOx_BSRR = pins;
    }
    else
    {
        port->GPIOx_BRR = pins;
    }
}

uint32 gpio_readPin(GPIOTypeDef *port, uint32 pin)
{
    uint32 position = 0;
    uint32 currentPin;

    while ( (pin >> position) != 0)
    {
        currentPin = pin & (1 << position);

        if (currentPin != 0)
        {
            break;
        } 
        position++;
    } 
    return (uint32)BIT_STATE(port->GPIOx_IDR , position );
}

uint32 gpio_readPort(GPIOTypeDef *port)
{
    return port->GPIOx_IDR;
}

void gpio_setPins(GPIOTypeDef *port, uint32 pins)
{
    port->GPIOx_BSRR = pins;
}

void gpio_isrHandler(uint32 pin)
{
    if (EXTI->PR & pin)
    {
        EXTI->PR |= pin;
        gpio_isrCallback( pin );
    }
}

weak void gpio_isrCallback(uint32 pin)
{

}