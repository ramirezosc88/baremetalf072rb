CC = arm-none-eabi-gcc
MACH = cortex-m0
OBJS = main.o stm32_startup.o gpio.o nvic.o uart.o
HEADERS = -I inc
VPATH = src
CFLAGS = -c -mcpu=$(MACH) -mthumb -mfloat-abi=soft -std=gnu11 -Wall -O0 -g3 $(HEADERS)
LDFLAGS = -mcpu=$(MACH) -mthumb -mfloat-abi=soft  --specs=rdimon.specs --specs=nano.specs -T stm32_ls.ld -Wl,-Map=final.map 


all : final.elf 
      
Output/obj/%.o : %.c
	mkdir -p Output/obj 
	@$(CC)  $(CFLAGS) -o $@ $<
	
final.elf : $(addprefix Output/obj/,$(OBJS))
	@$(CC) $(LDFLAGS) -o $@ $^

clean:
	@rm -rf final.elf final.map Output

open:
	@openocd -f board/st_nucleo_f0.cfg

debug:
	@arm-none-eabi-gdb final.elf -x=command.gdb 
	
flash:	
	openocd -f board/st_nucleo_f0.cfg -c "program final.elf verify reset exit"
